from pytrends.request import TrendReq
import sys

pytrends = TrendReq(hl='en-US', tz=360)
date = sys.argv[1]
year_trend = pytrends.top_charts(date, geo='ID')
print(year_trend['title'].to_list())
